import 'dart:core';
import 'dart:typed_data';
import 'package:libsodium/libsodium.dart';

class FieldElementException implements Exception
{
    String msg = "";


    FieldElementException(String msg)
    {
        this.msg = msg;

    }

    @override
    String toString()
    {
        return "Message: $msg\n";
    }

}

class FieldElement
{
    BigInt num;

    BigInt prime;

    FieldElement(this.num,this.prime)
    {
    }

    @override
    String toString()
    {
      return 'FieldElement_{${prime}}({${num}})';
    }

    BigInt getnum()
    {
        return this.num;
    }

    BigInt getprime()
    {
        return this.prime;
    }


    FieldElement modpow(BigInt exp)
    {
        while ( exp < BigInt.parse("0") )
        {
            exp += ( this.prime - BigInt.parse("1") );


        }

        return FieldElement(this.num.modPow(exp,this.prime),this.prime);
    }

    @override
    operator ==(rhs)
    {
        if ( rhs is! FieldElement )
        {
            return false;
        }

        if ( rhs == null )
        {
            return false;
        }

        return this.num == rhs.num && this.prime == rhs.prime;
    }

    @override
    FieldElement operator +(rhs)
    {
        try
        {
            if ( rhs is! FieldElement )
            {
                throw FieldElementException("in operator+(), rhs is not of type FieldElement");
            }
        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {
            if ( rhs == null )
            {
                throw FieldElementException("in operator+(), rhs ==  null\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {

            if ( this.prime != rhs.prime )
            {
                throw FieldElementException("in operator+(), this.prime != rhs.prime\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        return FieldElement( (this.num + rhs.num) % prime , this.prime );

    }


    @override
    FieldElement operator -(rhs)
    {
        try
        {
            if ( rhs is! FieldElement )
            {
                throw FieldElementException("in operator+(), rhs is not of type FieldElement");
            }
        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {
            if ( rhs == null )
            {
                throw FieldElementException("in operator+(), rhs ==  null\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {

            if ( this.prime != rhs.prime )
            {
                throw FieldElementException("in operator+(), this.prime != rhs.prime\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        return FieldElement( (this.num - rhs.num) % prime , this.prime );

    }

    @override
    FieldElement operator *(rhs)
    {
        try
        {
            if ( rhs is! FieldElement )
            {
                throw FieldElementException("in operator+(), rhs is not of type FieldElement");
            }
        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {
            if ( rhs == null )
            {
                throw FieldElementException("in operator+(), rhs ==  null\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {

            if ( this.prime != rhs.prime )
            {
                throw FieldElementException("in operator+(), this.prime != rhs.prime\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        return FieldElement( (this.num * rhs.num) % prime , this.prime );

    }

    @override
    FieldElement operator /(rhs)
    {
        try
        {
            if ( rhs is! FieldElement )
            {
                throw FieldElementException("in operator+(), rhs is not of type FieldElement");
            }
        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {
            if ( rhs == null )
            {
                throw FieldElementException("in operator+(), rhs ==  null\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        try
        {

            if ( this.prime != rhs.prime )
            {
                throw FieldElementException("in operator+(), this.prime != rhs.prime\n");
            }

        } on FieldElementException catch (e)
        {
            print(e.toString());
        }

        return FieldElement( ( this.num * rhs.num.modPow(this.prime-BigInt.parse("2"),this.prime) ) % this.prime , this.prime );

    }

}

BigInt uint8list_to_bigint(Uint8List input)
{
    int i = 0;

    const List<String> hextable = ["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"];

    String hexstring = "";

    while ( i < input.length )
    {

        hexstring += hextable[( ( input[i]  >> 4) & 0x0f )];

        hexstring += hextable[( ( input[i] ) & 0xf )];

        i++;
    }

    return BigInt.parse(hexstring,radix:16);
}


BigInt shamir_gen_constant(int entropy,BigInt p)
{
    BigInt one = BigInt.parse("1");

    int counter = 0;

    BigInt p_copy = p;

    BigInt res = BigInt.parse("0");

    while ( res <= BigInt.parse("0") )
    {
        p_copy = p;

        counter = 0;

        while ( p_copy > BigInt.parse("0") )
        {
            p_copy >>= 1;

            counter++;
        }

        try
        {

            if ( counter <= ( 2 * entropy ) )
            {
                throw FieldElementException("Error: ${counter} <= ( 2 * ${entropy} )\n");
            }

        }   on FieldElementException catch(e)
        {
            print(e.toString());
        }

        if ( counter % 8 != 0 )
        {
            counter = counter ~/ 8 + 1;
        }

        else
        {
            counter = counter ~/ 8;
        }

        int spanlen = counter;

        Uint8List uint8span = Sodium.randombytesBuf(spanlen);

        res = uint8list_to_bigint(uint8span);

        res = res % p;

    }

    return res;

}

BigInt reconstruction(int k,BigInt p,List<BigInt> x_vec,List<BigInt> pieces)
{
    FieldElement y_j = FieldElement(BigInt.parse("0"),p);

    FieldElement x_m = FieldElement(BigInt.parse("0"),p);

    FieldElement x_j = FieldElement(BigInt.parse("0"),p);

    FieldElement product = FieldElement(BigInt.parse("1"),p);

    FieldElement sum = FieldElement(BigInt.parse("0"),p);

    int j = 0, m = 0;

    while ( j <= (k-1) )
    {
        y_j = FieldElement(pieces[j],p);

        x_j = FieldElement(x_vec[j],p);

        product = FieldElement(BigInt.parse("1"),p);

        m = 0;

        while ( m <= (k-1) )
        {
            if ( m == j )
            {
                m++;

                continue;
            }

            x_m = FieldElement(x_vec[m],p);

            product = product * ( x_m / ( x_m - x_j ) );

            m++;
        }

        sum = sum + y_j * product;

        j++;
    }

    return sum.getnum();

}

List<BigInt> replacement(
                            int k

                            ,

                            BigInt p

                            ,

                            int entropy

                            ,

                            List<BigInt> a_vec

                            ,

                            List<BigInt> x_vec

                            ,

                            List<BigInt> pieces

                        )
{
        BigInt reconstruct = reconstruction(k,p,x_vec,pieces);

        BigInt x_i = shamir_gen_constant(entropy,p);

        BigInt y_i = shamir_gen_piece(k,p,reconstruct,x_i,a_vec);

        List<BigInt> coordinate = List<BigInt>(2);

        coordinate[0] = x_i;

        coordinate[1] = y_i;

        return coordinate;
}



BigInt shamir_gen_piece(int k,BigInt p,BigInt a_0,BigInt x_i,List<BigInt> a_i)
{

    FieldElement calc = FieldElement(a_0,p);

    FieldElement x_i_fieldelem = FieldElement(x_i,p);

    int i = 1;

    BigInt j = BigInt.parse("1");

    FieldElement a_i_fieldelem = FieldElement(BigInt.parse("0"),p);

    while ( i < (k-1) )
    {
        a_i_fieldelem = FieldElement(a_i[i],p);

        calc = calc + a_i_fieldelem * x_i_fieldelem.modpow( j );

        i++;

        j += BigInt.parse("1");
    }

    return calc.getnum();

}

List<List<BigInt>> shamir_format_piece_list(int k,int n,int entropy,BigInt p,BigInt a_0)
{
    try
    {
        if ( k >= n )
        {
            throw new FieldElementException("Error: k >= n: ${k} >= ${n}\n");
        }
    }   on FieldElementException catch(e)
    {
        print(e.toString());
    }

    List<BigInt> result = List<BigInt>(n);

    List<BigInt> a_vec = List<BigInt>(k-1);

    List<BigInt> x_vec = List<BigInt>(n);

    int a_index = 0;

    while ( a_index < (k-1) )
    {
        a_vec[a_index] = shamir_gen_constant(entropy,p);

        a_index++;
    }

    a_index = 0;

    int x_index = 0;

    while ( x_index < n )
    {
        x_vec[x_index] = shamir_gen_constant(entropy,p);

        x_index++;
    }

    a_index = 0;

    while ( a_index < n )
    {
        result[a_index] = shamir_gen_piece(k,p,a_0,x_vec[a_index],a_vec);

        a_index++;
    }

    a_index = 0;

    while ( a_index < result.length )
    {
        try
        {
            if ( result[a_index] >= p )
            {
                throw FieldElementException("Error: result[a_index] >= p: ${result[a_index]} >= ${p}");
            }
        } on FieldElementException catch(e)
        {
            print(e.toString());
        }

        a_index++;
    }

    List<List<BigInt>> final_form = List<List<BigInt>>(3);

    final_form[0] = a_vec;

    final_form[1] = x_vec;

    final_form[2] = result;

    return final_form;
}


/*
  FieldElement a = FieldElement( BigInt.parse("4") , BigInt.parse("31") );

  FieldElement b = FieldElement( BigInt.parse("11") , BigInt.parse("31") );


  print(a==b);

  print(a+b);

  print(a-b);

  print(a*b);

  print( a.modpow( BigInt.parse( "-4") ) * b );

  Sodium.init();

  final password = 'my password';

  final str = PasswordHash.hashStringStorage(password);

  print(str);

  Uint8List x = Sodium.randombytesBuf(32);

  for ( int i = 1; i < x.length ; i++ )
  {
      x[i] = 0x00;
  }

  x[0] = 0xff;

  x[31] = 0xff;

  BigInt s = BigInt.parse("ff000000000000000000000000000000000000000000000000000000000000ff",radix: 16);

  print(uint8list_to_bigint(x) == s);

  // next make a function that first converts Uint8List to String and
  // converts that to BigInt using the BigInt.parse() method radix 16
*/

void test_reconstruction()
{
    Uint8List x = Sodium.randombytesBuf(32);

    BigInt a_0 = uint8list_to_bigint(x);

    BigInt p = BigInt.parse("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",radix: 2);

    List<List<BigInt>> format_list = shamir_format_piece_list(3,5,256,p,a_0);

//    print("a_vec: ${format_list[0]}\n");

//    print("x_vec: ${format_list[1]}\n");

//    print("result: ${format_list[2]}\n");

    List<BigInt> a_vec = format_list[0];

    List<BigInt> x_vec = format_list[1];

    List<BigInt> result = format_list[2];

    BigInt reconstruct = reconstruction(3,p,x_vec,result);

    print("reconstruct[0..3]: ${reconstruct == a_0}\n");

    reconstruct = reconstruction(3,p,x_vec.sublist(1,4),result.sublist(1,4));

    print("reconstruct[1..3]: ${reconstruct == a_0}\n");

    reconstruct = reconstruction(3,p,x_vec.sublist(2,5),result.sublist(2,5));

    print("reconstruct[2..4]: ${reconstruct == a_0}\n");

}

List<List<BigInt>> redefinition(
                            int k

                            ,

                            int n

                            ,

                            BigInt p

                            ,

                            int entropy

                            ,

                            List<BigInt> x_vec

                            ,

                            List<BigInt> pieces

                        )
{
      BigInt reconstruct_secret = reconstruction(k,p,x_vec,pieces);

      List<List<BigInt>> new_result =  shamir_format_piece_list(k,n,entropy,p,reconstruct_secret);

      return new_result;
}

void test_redefinition()
{
    Uint8List x = Sodium.randombytesBuf(32);

    BigInt a_0 = uint8list_to_bigint(x);

    BigInt p = BigInt.parse("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",radix: 2);

    BigInt reconstruct_secret = BigInt.parse("0");

    List<List<BigInt>> format_list = shamir_format_piece_list(3,5,256,p,a_0);

    List<BigInt> a_vec = format_list[0];

    List<BigInt> x_vec = format_list[1];

    List<BigInt> pieces = format_list[2];

    List<List<BigInt>> redefined_list = redefinition(3,5,p,256,x_vec,pieces);

    int i = 0;

    while ( i < 30 )
    {
        redefined_list = redefinition(3,5,p,256,x_vec,pieces);

        reconstruct_secret = reconstruction(3,p,x_vec,pieces);

        print("reconstruct_secret == a_0: ${reconstruct_secret == a_0}");

        i++;
    }

}

void test_replacement()
{
    Uint8List x = Sodium.randombytesBuf(32);

    BigInt a_0 = uint8list_to_bigint(x);

    BigInt p = BigInt.parse("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111",radix: 2);

    List<List<BigInt>> format_list = shamir_format_piece_list(3,5,256,p,a_0);

    BigInt reconstruct = reconstruction(3,p,format_list[1],format_list[2]);

    List<BigInt> a_vec = format_list[0];

    List<BigInt> x_vec = format_list[1];

    List<BigInt> pieces = format_list[2];

    List<BigInt> rep = replacement(3,p,256,a_vec,x_vec,pieces);

    x_vec[0] = rep[0];

    pieces[0] = rep[1];

    BigInt reconstruct_replacement = reconstruction(3,p,x_vec,pieces);

    print("${reconstruct_replacement == a_0}\n");

}

void main()
{

    int i = 0;

    while ( i < 30 )
    {
        test_redefinition();

        i++;
    }


}
